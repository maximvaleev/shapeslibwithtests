namespace ShapesLibrary.Tests;

[TestClass]
public class CircleTests
{
    [TestMethod]
    public void CalculateArea_Radius10_314returned()
    {
        // Arrange
        double radius = 10.0;
        IShape circle = new Circle(radius);
        double expected = 314.15926535;
        double tolerance = expected * 0.00001;
        // Act
        double actual = circle.CalculateArea();
        // Assert
        Assert.AreEqual(expected, actual, tolerance);
    }
}