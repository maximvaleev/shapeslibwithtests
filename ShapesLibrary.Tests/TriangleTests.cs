﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapesLibrary.Tests;

[TestClass]
public class TriangleTests
{
    [TestMethod]
    public void CalculateArea_Sides3and4and5_6returned()
    {
        // Arrange
        IShape triangle = new Triangle(3.0, 4.0, 5.0);
        double expected = 6.0;
        double tolerance = expected * 0.00001;
        // Act
        double actual = triangle.CalculateArea();
        // Assert
        Assert.AreEqual(expected, actual, tolerance);
    }

    [TestMethod]
    public void Constructor_InvalidSides1and2and5_ThrowsException()
    {
        // Arrange
        Action createInvalidTriangle = () => new Triangle(1.0, 2.0, 5.0);
        // Act
        // Assert
        Assert.ThrowsException<ArgumentException>(createInvalidTriangle);
    }

    [TestMethod]
    public void IsRightTriangle_Sides3and4and5_TrueReturns()
    {
        // Arrange
        Triangle triangle = new (3.0, 4.0, 5.0);
        // Act
        bool actual = triangle.IsRightTriangle();
        // Assert
        Assert.IsTrue(actual);
    }

    [TestMethod]
    public void IsRightTriangle_Sides5and6and7_FalseReturns()
    {
        // Arrange
        Triangle triangle = new (5.0, 6.0, 7.0);
        // Act
        bool actual = triangle.IsRightTriangle();
        // Assert
        Assert.IsFalse(actual);
    }
}
