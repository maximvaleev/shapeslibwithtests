﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShapesLibrary;

public static class ShapesCalculator
{
    public static double GetTriangleArea(double sideA, double sideB, double sideC)
    {
        return new Triangle(sideA, sideB, sideC).CalculateArea();
    }

    public static double GetCircleArea(double radius)
    {
        return new Circle(radius).CalculateArea();
    }

    public static bool IsRightTriangle(double sideA, double sideB, double sideC)
    {
        return new Triangle(sideA, sideB, sideC).IsRightTriangle();
    }
}
