﻿
namespace ShapesLibrary;

public class Circle : IShape
{
    public double Radius { get; private set; }

    public Circle(double radius)
    {
        Radius = Math.Abs(radius);
    }

    public double CalculateArea()
    {
        return Math.PI * Radius * Radius;
    }
}
