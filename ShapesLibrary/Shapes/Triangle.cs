﻿
namespace ShapesLibrary;
public class Triangle : IShape
{
    public double SideA { get; private set; }
    public double SideB { get; private set; }
    public double SideC { get; private set; }

    public Triangle(double sideA, double sideB, double sideC)
    {
        SideA = Math.Abs(sideA);
        SideB = Math.Abs(sideB);
        SideC = Math.Abs(sideC);

        if (!IsValid())
        {
            throw new ArgumentException("Invalid triangle sides");
        }
    }

    private bool IsValid()
    {
        if (SideA > SideB + SideC || SideB > SideA + SideC || SideC > SideA + SideB)
        {
            return false;
        }
        return true;
    }

    public double CalculateArea()
    {
        double p = (SideA + SideB + SideC) / 2d;
        return Math.Sqrt(p * (p - SideA) * (p - SideB) * (p - SideC));
    }

    public bool IsRightTriangle()
    {
        double[] sides = { SideA, SideB, SideC };
        Array.Sort(sides);

        double tolerance = sides[2] * 0.00001;

        return Math.Abs(sides[2] * sides[2] - sides[1] * sides[1] - sides[0] * sides[0]) < tolerance;
    }
}
