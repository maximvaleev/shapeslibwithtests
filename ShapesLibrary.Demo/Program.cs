﻿using ShapesLibrary;

namespace ShapesLibrary.Demo;

internal class Program
{
    static void Main()
    {
        double circleRadius = 10.0;
        double triSideA = 2.0, triSideB = 3.0, triSideC = 4.0;

        IShape circle = new Circle(circleRadius);
        IShape triangle = new Triangle(triSideA, triSideB, triSideC);

        Console.WriteLine($"Площадь круга с радиусом {circleRadius} = {circle.CalculateArea()}");
        Console.WriteLine($"Площадь треугольника со сторонами {triSideA}, {triSideB}, {triSideC} " +
            $"= {triangle.CalculateArea()}");
        Console.WriteLine("Этот треугольник " + 
            (((Triangle)triangle).IsRightTriangle() ? "прямоугольный" : "не прямоугольный"));

        Console.WriteLine("\n*** Теперь с использованием статического класса для упрощения:");
        Console.WriteLine($"Площадь круга с радиусом {circleRadius} = {ShapesCalculator.GetCircleArea(circleRadius)}");
        Console.WriteLine($"Площадь треугольника со сторонами {triSideA}, {triSideB}, {triSideC} " +
            $"= {ShapesCalculator.GetTriangleArea(triSideA, triSideB, triSideC)}");
        Console.WriteLine("Этот треугольник " +
            (ShapesCalculator.IsRightTriangle(triSideA, triSideB, triSideC) ? "прямоугольный" : "не прямоугольный"));
    }
}
